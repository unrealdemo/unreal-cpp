// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "CppPuzzleGameGameMode.generated.h"

/** GameMode class to specify pawn and playercontroller */
UCLASS(minimalapi)
class ACppPuzzleGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACppPuzzleGameGameMode();
};



