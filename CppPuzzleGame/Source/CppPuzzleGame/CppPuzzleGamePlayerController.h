// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/PlayerController.h"
#include "CppPuzzleGamePlayerController.generated.h"

/** PlayerController class used to enable cursor */
UCLASS()
class ACppPuzzleGamePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ACppPuzzleGamePlayerController();
};


