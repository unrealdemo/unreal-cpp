// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "CppPuzzleGame.h"
#include "CppPuzzleGamePlayerController.h"

ACppPuzzleGamePlayerController::ACppPuzzleGamePlayerController()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}
