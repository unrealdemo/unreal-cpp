// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "CppPuzzleGame.h"
#include "CppPuzzleGameGameMode.h"
#include "CppPuzzleGamePlayerController.h"
#include "CppPuzzleGamePawn.h"

ACppPuzzleGameGameMode::ACppPuzzleGameGameMode()
{
	// no pawn by default
	DefaultPawnClass = ACppPuzzleGamePawn::StaticClass();
	// use our own player controller class
	PlayerControllerClass = ACppPuzzleGamePlayerController::StaticClass();
}
