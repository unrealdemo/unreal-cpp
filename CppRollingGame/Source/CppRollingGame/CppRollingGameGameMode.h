// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "CppRollingGameGameMode.generated.h"

UCLASS(minimalapi)
class ACppRollingGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACppRollingGameGameMode();
};



