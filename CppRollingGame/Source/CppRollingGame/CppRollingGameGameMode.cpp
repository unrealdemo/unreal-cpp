// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "CppRollingGame.h"
#include "CppRollingGameGameMode.h"
#include "CppRollingGameBall.h"

ACppRollingGameGameMode::ACppRollingGameGameMode()
{
	// set default pawn class to our ball
	DefaultPawnClass = ACppRollingGameBall::StaticClass();
}
