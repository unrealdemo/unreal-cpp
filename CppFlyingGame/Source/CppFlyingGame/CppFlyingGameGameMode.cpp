// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "CppFlyingGame.h"
#include "CppFlyingGameGameMode.h"
#include "CppFlyingGamePawn.h"

ACppFlyingGameGameMode::ACppFlyingGameGameMode()
{
	// set default pawn class to our flying pawn
	DefaultPawnClass = ACppFlyingGamePawn::StaticClass();
}
