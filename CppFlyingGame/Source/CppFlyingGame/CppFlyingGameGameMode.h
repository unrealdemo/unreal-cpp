// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "CppFlyingGameGameMode.generated.h"

UCLASS(minimalapi)
class ACppFlyingGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACppFlyingGameGameMode();
};



