// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "CppFirstPersonGame.h"
#include "CppFirstPersonGameGameMode.h"
#include "CppFirstPersonGameHUD.h"
#include "CppFirstPersonGameCharacter.h"

ACppFirstPersonGameGameMode::ACppFirstPersonGameGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ACppFirstPersonGameHUD::StaticClass();
}
