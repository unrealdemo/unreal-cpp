// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "CppFirstPersonGameGameMode.generated.h"

UCLASS(minimalapi)
class ACppFirstPersonGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACppFirstPersonGameGameMode();
};



