// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "CppSideScrollerGameGameMode.generated.h"

UCLASS(minimalapi)
class ACppSideScrollerGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACppSideScrollerGameGameMode();
};



